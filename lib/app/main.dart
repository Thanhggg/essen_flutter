import 'package:essen_flutter/app/common/animation/Animations.dart';
import 'package:essen_flutter/app/common/theme/theme.dart';
import 'package:essen_flutter/app/mainBloc.dart';
import 'package:essen_flutter/core/app/app.dart';
import 'package:essen_flutter/core/bloc/bloc.dart';
import 'package:essen_flutter/core/core.dart';
import 'package:flutter/material.dart';

import 'screen/child_registration/childRegister.dart';
import 'screen/child_registration/childRegisterBloc.dart';
import 'screen/child_selection/childSelection.dart';
import 'screen/child_selection/childSelectionBloc.dart';
import 'screen/splash/splash.dart';
import 'screen/splash/splashBloc.dart';

class MyApp extends BaseApp {
  final String title;
  final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();
  MyApp(this.title) : super(title);

  @override
  void onInitFeature(BuildContext context) {
    FlutBus.init();
  }

  @override
  Widget rootBuilder(BuildContext context) {
    return BlocProvider(
      bloc: MainBloc(),
      widget: MaterialApp(
        title: 'Flutter Demo',
        theme: AppThemes.appTheme,
        onGenerateRoute: (setting) {
          switch (setting.name) {
            case '/':
              return PageRouteBuilder(
                  pageBuilder: (_, __, ___) => BlocProvider(
                        bloc: SplashBloc(),
                        widget: SplashPage(),
                      ));
            case '/ChildSelection':
              return Animations.slideTransitionBuilder(
                  context: context,
                  child: BlocProvider(
                    bloc: ChildSelectionBloc(),
                    widget: ChildSelectionPage(),
                  ));
            case '/ChildRegister':
              return Animations.slideTransitionBuilder(
                  context: context,
                  child: BlocProvider(
                    bloc: ChildRegisterBloc(),
                    widget: ChildRegisterPage(),
                  ));
            default:
              return PageRouteBuilder(
                  pageBuilder: (_, __, ___) => BlocProvider(
                        bloc: SplashBloc(),
                        widget: SplashPage(),
                      ));
          }
        },
        initialRoute: '/',
      ),
    );
  }
}

void main() => runApp(MyApp("Child Meal Management"));
