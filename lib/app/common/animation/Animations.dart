import 'package:flutter/cupertino.dart';

class Animations {
  Animations._internal();

  static PageRouteBuilder slideTransitionBuilder(
          {@required BuildContext context, @required Widget child}) =>
      PageRouteBuilder(
          pageBuilder: (context, inAnim, outAnim) => child,
          transitionDuration: Duration(milliseconds: 300),
          transitionsBuilder: (context, inAnim, outAnim, child) =>
              SlideTransition(
                child: child,
                position: inAnim.drive(
                    Tween(begin: Offset(1.0, 0.0), end: Offset.zero)
                        .chain(CurveTween(curve: Curves.easeIn))),
              ));

  static PageRouteBuilder presentTransitionBuilder(
          {@required BuildContext context, @required Widget child}) =>
      PageRouteBuilder(
          pageBuilder: (context, inAnim, outAnim) => child,
          transitionDuration: Duration(milliseconds: 300),
          transitionsBuilder: (context, inAnim, outAnim, child) =>
              FadeTransition(
                  opacity: inAnim.drive(Tween(begin: 0.0, end: 1.0)),
                  child: SlideTransition(
                    child: child,
                    position: inAnim.drive(
                        Tween(begin: Offset(0.0, 0.3), end: Offset.zero)
                            .chain(CurveTween(curve: Curves.easeIn))),
                  )));
}
