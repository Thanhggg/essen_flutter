import 'package:flutter/material.dart';

class Styles {
  static Gradient screenBackgroundGradient = const LinearGradient(
      tileMode: TileMode.clamp,
      colors: [Color(0xFF70e1f5), Color(0xFFffd194)],
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter);
}
