import 'dart:convert';

import 'package:essen_flutter/app/common/animation/Animations.dart';
import 'package:essen_flutter/app/common/theme/dimens.dart';
import 'package:essen_flutter/app/common/theme/styles.dart';
import 'package:essen_flutter/app/screen/child_detail/childDetailScreen.dart';
import 'package:essen_flutter/core/core.dart';
import 'package:essen_flutter/core/ui/common_view/observableWidget.dart';
import 'package:flutter/material.dart';

import 'childSelectionBloc.dart';
import 'childSelectionModel.dart';

class ChildSelectionPage extends BasePage<ChildSelectionBloc> {
  @override
  ChildSelectionBloc getBlocData(BuildContext context) {
    return BlocProviders.of<ChildSelectionBloc>(context);
  }

  @override
  void onInitEvent(BuildContext context) {
    NetWorkRequest().httpClient().post("https://reqres.in/api/login",
        body: json.encode({"email": "eve.holt@reqres.in", "password": "cityslicka"}),
        onComplete: (respond) {
      print("login_success: ${respond.body}");
    }, onFailed: (error) {
      print(error.message);
    });
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return Container(
        padding:
            EdgeInsets.fromLTRB(Dimension.pad20, 0.0, Dimension.pad20, 0.0),
        decoration: BoxDecoration(
            gradient: Styles.screenBackgroundGradient,
            shape: BoxShape.rectangle),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: Dimension.pad37,
            ),
            Text(
              "Select Child",
              style: Theme.of(context).textTheme.headline,
            ),
            SizedBox(
              height: Dimension.pad22,
            ),
            Expanded(
              child: ObservableWidget<List<dynamic>>(
                observable: bloc.childListObserField,
                onbuild: (context, data) => ListView.separated(
                  separatorBuilder: (context, position) =>
                      SizedBox(height: Dimension.pad16),
                  itemCount: data.length,
                  padding: EdgeInsets.only(
                      top: Dimension.pad5, bottom: Dimension.pad5),
                  itemBuilder: (context, position) {
                    var item = data[position];
                    if (item is ChildInfoModel)
                      return buildItem(context, data[position]);
                    else
                      return buildAddnewButton(context);
                  },
                ),
              ),
            )
          ],
        ));
  }

  Widget buildItem(BuildContext context, ChildInfoModel data) {
    return GestureDetector(
        onTap: () {
          getNavigator(context).push(Animations.presentTransitionBuilder(
              context: context, child: ChildDetailScreen()));
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Dimension.pad8),
              gradient: LinearGradient(colors: [
                Colors.accents[2],
                Colors.blue,
              ]),
              shape: BoxShape.rectangle,
              boxShadow: [
                BoxShadow(
                    spreadRadius: 2.0,
                    blurRadius: Dimension.pad14,
                    color: Colors.black26)
              ]),
          alignment: Alignment.center,
          padding: EdgeInsets.fromLTRB(Dimension.pad20, Dimension.pad10,
              Dimension.pad20, Dimension.pad10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(Dimension.pad120),
                  clipBehavior: Clip.antiAlias,
                  child: Image.asset(
                    "assets/images/profile_placeholder.png",
                    alignment: Alignment.center,
                    width: Dimension.pad120,
                    height: Dimension.pad120,
                  ),
                ),
              ),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Child: ${data.childName}",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.title,
                    ),
                    SizedBox(
                      height: Dimension.pad10,
                    ),
                    Text(
                      "Date of Birth: ${data.dateOfBirth}",
                      style: Theme.of(context).textTheme.subtitle,
                    )
                  ],
                ),
              )
            ],
          ),
        ));
  }

  Widget buildAddnewButton(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getNavigator(context).pushNamed('/ChildRegister');
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.blue[200],
            borderRadius: BorderRadius.circular(Dimension.pad6),
            boxShadow: [
              BoxShadow(
                  spreadRadius: 2.0,
                  blurRadius: Dimension.pad14,
                  color: Colors.black12)
            ]),
        padding: EdgeInsets.all(Dimension.pad10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.add,
              color: Colors.white60,
              size: Dimension.pad80,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onDispose() {
    print("ChildSelection: dispose");
    super.onDispose();
  }
}
