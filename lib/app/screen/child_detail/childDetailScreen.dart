import 'package:flutter/material.dart';

class ChildDetailScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ChildDetailState();
  }
}

class ChildDetailState extends State<ChildDetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Text(
        "Child Detail",
        style: Theme.of(context).textTheme.body1,
      ),
    ));
  }

  @override
  void dispose() {
    print("ChildDetail: onDispose");
    super.dispose();
  }
}
