import 'dart:async';

import 'package:essen_flutter/app/common/theme/styles.dart';
import 'package:essen_flutter/app/screen/child_selection/childSelection.dart';
import 'package:essen_flutter/app/screen/child_selection/childSelectionBloc.dart';
import 'package:essen_flutter/core/bloc/bloc_provider.dart';
import 'package:essen_flutter/core/ui/common_view/observableWidget.dart';
import 'package:essen_flutter/core/ui/screen/base_page.dart';
import 'package:flutter/material.dart';

import 'splashBloc.dart';

class SplashPage extends BasePage<SplashBloc> {
  static final stateKey = GlobalKey<CommonPageState>();
  SplashPage({NavigatorState navigator})
      : super(key: stateKey, navigator: navigator);

  AnimationController animationController;
  Tween<double> tween;
  @override
  SplashBloc getBlocData(BuildContext context) {
    return BlocProviders.of<SplashBloc>(context);
  }

  @override
  void onPageInit() {
    animationController = AnimationController(
        vsync: pageKey.currentState,
        duration: Duration(seconds: 3),
        reverseDuration: Duration(seconds: 3));
    tween = Tween<double>(begin: -4, end: 4);
    var animateValue = animationController.drive(tween);

    animationController.addListener(() {
      bloc.logoWidthObsevable.set(animateValue.value);
    });

    animationController.repeat(reverse: true);
    super.onPageInit();
  }

  @override
  void onInitEvent(BuildContext context) {
    Timer(Duration(seconds: 2), () {
      getNavigator(context).pushReplacementNamed('/ChildSelection');
    });
  }

  @override
  void onDispose() {
    animationController.dispose();
    super.onDispose();
  }

  @override
  Widget onPageBuild(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          shape: BoxShape.rectangle, gradient: Styles.screenBackgroundGradient),
      position: DecorationPosition.background,
      child: Container(
        margin: EdgeInsets.all(20.0),
        child: ObservableWidget<double>(
          observable: bloc.logoWidthObsevable,
          onbuild: (context, data) => Transform(
              transform: Matrix4.rotationY(data),
              alignment: Alignment.center,
              child: Image.asset(
                "assets/images/splash_logo.png",
                alignment: Alignment.center,
                fit: BoxFit.fitWidth,
              )),
        ),
      ),
    );
  }
}
