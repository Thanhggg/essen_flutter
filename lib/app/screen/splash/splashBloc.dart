import 'package:essen_flutter/core/bloc/base_bloc_component.dart';
import 'package:essen_flutter/core/event/observable.dart';

class SplashBloc extends BaseBloc {
  var logoWidthObsevable = ObservableField<double>(500);

  @override
  void dispose() {
    logoWidthObsevable.close();
    super.dispose();
  }
}
