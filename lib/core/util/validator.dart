import '../util/string_util.dart';

class Validator {
  static bool isEmail(String text) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    return regex.hasMatch(text);
  }

  static bool isLesserThan(String text, int number) {
    return StringUtil.isEmptyTrim(text) || text.length < number;
  }

  static bool isGreaterThan(String text, int number) {
    return !StringUtil.isEmptyTrim(text) && text.length > number;
  }

  static bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }

  static bool isInteger(String s) {
    if (s == null) {
      return false;
    }
    return int.parse(s) != null && int.parse(s) >= 0;
  }
}
