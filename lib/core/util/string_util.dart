class StringUtil {
  static bool isEmptyTrim(String text) {
    if (text == null) {
      return true;
    }

    if (text.trim().isEmpty) {
      return true;
    }

    return false;
  }

}
