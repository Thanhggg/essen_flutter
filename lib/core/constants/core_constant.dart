class CoreConstant {
  static const String DUMMY_URL =
      'https://jsonplaceholder.typicode.com/todos/1';
  static const int TRANSITION_ANIM_TIME = 300;
  static const String PREF_REFRESH_TOKEN = 'PREF_REFRESH_TOKEN';
  static const String PREF_ACCESS_TOKEN = 'PREF_ACCESS_TOKEN';

  static const String COMMON_ERROR_MSG = 'Error.';

  static const String REQUEST_LOGIN = 'REQUEST_LOGIN';
}

class CoreEvent {
  static const String REFRESH_TOKEN_FAILED = 'REFRESH_TOKEN_FAILED';
  static const String SHOW_LOADING = 'SHOW_LOADING';
  static const String SHOW_ERROR_DIALOG = 'SHOW_ERROR_DIALOG';
  static const String LOGIN_SUCCESS = 'LOGIN_SUCCESS';
}

