import 'package:http/http.dart';

abstract class Intercepter {
  Process intercep(Response respond);
}

enum Process {
  Retry,
  End,
  Abort,
  Chain,
}
