import 'package:http/http.dart';

import '../../../core/network/interface/intercepter.dart';

class TempIntercepter extends Intercepter {
  @override
  Process intercep(Response respond) {
    print('intercepter1');
    return Process.Retry;
  }
}

class TempIntercepter2 extends Intercepter {
  @override
  Process intercep(Response respond) {
    print('intercepter 2');
    return Process.Chain;
  }
}

class TempIntercepter3 extends Intercepter {
  @override
  Process intercep(Response respond) {
    print('intercepter 3');
    return Process.End;
  }
}
