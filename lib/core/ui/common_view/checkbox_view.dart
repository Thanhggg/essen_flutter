import 'dart:async';

import 'package:flutter/material.dart';
import '../../util/string_util.dart';

class CheckboxView extends StatelessWidget {
  final StreamController controller;
  final Map<String, bool> initData;

  CheckboxView(this.controller, this.initData);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: controller.stream,
      initialData: initData,
      builder: (context, snapshot) => Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: initData.keys
                          .where((item) =>
                              initData.keys.toList().indexOf(item) % 2 == 0)
                          .toList()
                          .map((item) => Row(
                                children: <Widget>[
                                  Checkbox(
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    value: initData[item],
                                    onChanged: (isCheck) {
                                      initData[item] = isCheck;
                                      controller.add(initData);
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(item),
                                  )
                                ],
                              ))
                          .toList()),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: initData.keys
                          .where((item) =>
                              initData.keys.toList().indexOf(item) % 2 == 1)
                          .toList()
                          .map((item) => Row(
                                children: <Widget>[
                                  Checkbox(
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    value: initData[item],
                                    onChanged: (isCheck) {
                                      initData[item] = isCheck;
                                      controller.add(initData);
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 5),
                                    child: Text(item),
                                  )
                                ],
                              ))
                          .toList()),
                ],
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(top: 10),
                child: snapshot != null &&
                        snapshot.hasError &&
                        !StringUtil.isEmptyTrim(snapshot.error)
                    ? Text(
                        '${snapshot.error}',
                        style: TextStyle(color: Colors.red),
                      )
                    : SizedBox(
                        height: 0,
                        width: 0,
                      ),
              )
            ],
          ),
    );
  }
}
