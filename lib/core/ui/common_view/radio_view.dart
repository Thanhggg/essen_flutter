import 'package:flutter/material.dart';
import '../../core.dart';

class RadioboxView extends StatelessWidget {
  final StreamController checkBoxController;
  final List<String> checkBoxLabels;
  final int checkPosition;

  RadioboxView(this.checkBoxLabels,
      {this.checkBoxController, this.checkPosition});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: checkPosition,
      stream: checkBoxController.stream,
      builder: (context, snapshot) => Container(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Column(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: this
                      .checkBoxLabels
                      .map((label) => Row(
                            children: <Widget>[
                              Text('$label: '),
                              SizedBox(
                                width: 30,
                                height: 30,
                                child: Radio(
                                  groupValue:
                                      snapshot != null && snapshot.hasData
                                          ? snapshot.data as int
                                          : -1,
                                  value: checkBoxLabels.indexOf(label),
                                  onChanged: (isCheck) {
                                    checkBoxController
                                        .add(checkBoxLabels.indexOf(label));
                                  },
                                ),
                              ),
                            ],
                          ))
                      .toList(),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 10),
                  child: snapshot != null &&
                          snapshot.hasError &&
                          !StringUtil.isEmptyTrim(snapshot.error)
                      ? Text(
                          '${snapshot.error}',
                          style: TextStyle(color: Colors.red),
                        )
                      : SizedBox(
                          height: 0,
                          width: 0,
                        ),
                )
              ],
            ),
          ),
    );
  }
}
