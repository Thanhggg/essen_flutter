import 'package:flutter/material.dart';

class ErrorDialogView extends StatelessWidget {
  final String _errorMessage;

  ErrorDialogView(this._errorMessage);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(
        this._errorMessage,
        textAlign: TextAlign.center,
      ),
      actions: <Widget>[
        RaisedButton(
          child: Text(
            "Close",
            style: TextStyle(
              color: Colors.white,
            ),
            textDirection: TextDirection.ltr,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
