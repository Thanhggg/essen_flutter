import 'package:essen_flutter/core/core.dart';
import 'package:flutter/material.dart';

abstract class BasePageController extends StatefulWidget {
  final Key key;
  BasePageController({this.key}) : super(key: key);
  String getPageName() => this.toStringShort();
}

abstract class BasePage<T extends BaseBloc> extends BasePageController {
  final BasePageProperties _properties = BasePageProperties();
  BasePage({Key key, NavigatorState navigator}) : super(key: key) {
    _properties.navigator = navigator;
    _properties.pageKey = key;
  }
  @override
  State<StatefulWidget> createState() {
    return CommonPageState();
  }

  Widget onPageBuild(BuildContext context);

  void onInitEvent(BuildContext context);

  void _onInitBlocData(BuildContext context) {
    _properties.bloc = getBlocData(context);
  }

  T getBlocData(BuildContext context);

  T get bloc => this._properties.bloc;

  GlobalKey<CommonPageState> get pageKey => _properties.pageKey;

  void onDispose() {}

  void onPageInit() {}

  NavigatorState getNavigator(BuildContext context) {
    if (this._properties.navigator == null)
      return Navigator.of(context);
    else
      return this._properties.navigator;
  }
}

class CommonPageState extends State<BasePage>
    with SingleTickerProviderStateMixin {
  static const String _INIT_EVENT_MSG = 'init event';
  StreamSubscription _initEventSubscription;
  var _didChangeDependency = false;

  @override
  void initState() {
    print("${widget.getPageName()} onStateInit");
    _initEventSubscription =
        FlutBus.listenDistinct<BuildContext>(_INIT_EVENT_MSG, (context) {
      print("${widget.getPageName()} onInitEvent");
      widget.onInitEvent(context);
      _initEventSubscription.cancel();
      return;
    });

    widget.onPageInit();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_didChangeDependency) {
      print("${widget.getPageName()} onInitBlocProvider");
      widget._onInitBlocData(context);
      FlutBus.pushEvent(_INIT_EVENT_MSG, context);
      _didChangeDependency = true;
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    print("${widget.getPageName()} onDispose");
    widget.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("${widget.getPageName()} onPageBuild");
    return widget.onPageBuild(context);
  }
}

class BasePageProperties<T extends BaseBloc> {
  T bloc;
  NavigatorState navigator;
  Key pageKey;
}
