import 'package:essen_flutter/core/core.dart';

class ObservableField<T> {
  T _value;
  Object _error;
  StreamController<T> _controller;

  ObservableField([T initValue]) {
    _value = initValue;
    _controller = StreamController.broadcast();
    _controller.stream.listen((data) {
      _value = data;
    }, onError: (error) {
      _error = error;
    });
  }

  void set(T value) {
    _controller.sink.add(value);
  }

  Stream<T> get() => this._controller.stream;

  T value() => this._value;

  void setError(dynamic error) {
    this._error = error;
  }

  dynamic errorValue() => this._error;

  void close() {
    _controller.close();
  }
}
