import 'dart:async';

import 'package:rxdart/rxdart.dart';

class LiveData<T> {
  BehaviorSubject<T> _subject;
  StreamSubscription<T> _listenSubScription;
  bool _isListen = false;

  LiveData() {
    _subject = BehaviorSubject<T>();
  }

  void postValue(T value) {
    _subject.sink.add(value);
  }

  void postError(Object error) {
    _subject.addError(error);
  }

  StreamSubscription<T> observerValue(OndataObserver dataObserver,
      {OnErrorValue onErrorValue}) {
    if (!_isListen) {
      _listenSubScription = _subject.listen((data) {
        dataObserver(data);
      }, onError: onErrorValue);
      _isListen = true;
    }

    return _listenSubScription;
  }

  Stream get stream => _subject.stream;

  Future<dynamic> close() {
    print('LiveData close');
    _listenSubScription?.cancel();
    return _subject.close();
  }
}

typedef void OndataObserver<T>(T value);
typedef void OnErrorValue(dynamic params);
