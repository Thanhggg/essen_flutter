import 'dart:async';

class Disposable<T> {
  StreamSubscription<T> _streamSubscription;

  set subcription(StreamSubscription<T> streamSubscription) =>
      this._streamSubscription = streamSubscription;

  void close() {
    _streamSubscription?.cancel();
  }
}
