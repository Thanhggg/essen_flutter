import 'dart:convert';

import 'package:http/http.dart';

class RespondModel {
  int _code;
  Map<String, dynamic> _data;
  String message;
  RespondModel(this._code, this._data, {this.message});

  set code(int code) => this._code = code;
  get code => this._code;

  get body => this._data;
  set body(Map<String, dynamic> body) => this._data = body;

  factory RespondModel.generate(Response respond) {
    return RespondModel(respond.statusCode, jsonDecode(respond.body));
  }
}
